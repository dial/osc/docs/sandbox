.. DIAL Online Catalog documentation master file, created by
   sphinx-quickstart on Fri Jan 10 14:24:14 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Digital Impact Alliance Online Catalog
======================================

DIAL's Online Catalog is a resource for T4D practitioners to discover and evaluate 
open source digital public goods that are oriented toward achieving the SDGs. 
The catalog is built to support DIAL's `Digital Investment Framework`_

.. _Digital Investment Framework: https://digitalimpactalliance.org/research/sdg-digital-investment-framework/

.. toctree::
   :maxdepth: 2
   :caption: Contents:


How to use the catalog
----------------------

DIAL's online catalog is designed to be flexible and give users multiple starting points
for discovering and evaluating products. The catalog has a powerful filtering system that
allows users to clearly see and understand how Sustainable Development Goals, Use Cases, 
Workflows, Building Blocks and products connect. 

On the main page of the application, the user will see a navigation bar:

.. image:: img/topnav.png
   :width: 600

This navigation bar allows the user to see the various :doc:`sustainable development goals <sdg>`,
:doc:`use cases <use_case>`, :doc:`workflows <workflow>`, :doc:`building blocks <building_block>`,
and :doc:`products <product>` in the catalog. Click on any section, and the main window will display
the entries for that category. 

The :doc:`filtering <filter>` functionality allows the user to select the use cases, SDGs, 
building blocks or specific products that they are interested in and see the items that match
that filtering criteria. For example, if a user selects a specific SDG, the catalog will show 
the use cases, workflows, building blocks, and products that are oriented toward addressing
that SDG. See the :doc:`filtering <filter>` section for more information.

The online catalog also tracks :doc:`organizations <organization>`. These organizations
are NGOs and non-profits that have endorsed DIAL's `Digital Principles`_ and are working around
the world. Users can see other organizations working in their target geographies. 

.. _Digital Principles: https://digitalprinciples.org

Finally, the catalog tracks mobile network aggregators (part of the organizations page). This 
allows users to see what mobile network services are available in different countries.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: ICT4SDG Framework

   sdg
   use_case
   workflow
   building_block
   product

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Organizations

   organization
   map_view

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Filtering

   filter

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Glossary/Definitions

   glossary

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Install/Configure

   install