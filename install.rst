Installing and Configuring the Catalog
======================================

.. meta::
   :description lang=en:

This catalog can be deployed by any organization that wishes to curate their own
list of digital public goods. All of the code is publicly available on the 
`Product Registry repository`_

**Please note that these instructions are for users who have a technical background and are familiar with Rails applications and Docker deployments**

.. _Product Registry repository: https://gitlab.com/dial/t4d-product-backlog/product-registry

To configure and run an instance of the online registry/catalog, perform the following steps:

 * Clone the product-registry repository into a directory on your server or development environment
 * Configure environment variables for the application. The application requires several variables to be set in your environment. We have provided a sample shell script that will set the appropriate variables for development, test, or production environments. The variables are as follows:

  * SECRET_KEY_BASE - set a unique secret or password. This is used for user authentication verification
  * DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD - connecting data for the Postgres database. Ensure that these values match the settings in the docker-compose.yml file for the database instance (described below)
  * TEST_DATABASE_NAME, TEST_DATABASE_USER, TEST_DATABASE_PASSWORD - to run the unit or integration tests, these values need to be set and point to a test database instance
  * ESRI_CLIENT_ID, ESRI_CLIENT_SECRET - the application uses `ESRI`_ for geographic data. You can sign up for an account and access token.
  * GOOGLE_TRACKER - insert a tracking id to use Google analytics
  * JENKINS_URL, JENKINS_USER, JENKINS_PASSWORD - in order to use the product launcher functionality, the application must connect to a running Jenkins instance. 
  * MAILGUN_API_KEY, MAILGUN_DOMAIN - the application uses mailgun to send email notifications to users. Use mailgun account data here to enable this functionality
  * CAPTCHA_SITE_KEY, CAPTCHA_SECRET_KEY - the main signup page uses Captcha. Enter your captcha credentials to use this functionality
  * GITHUB_USERNAME, GITHUB_PERSONAL_TOKEN - the appication will automatically read data from any GitHub repositories that are connected to products. In order to do that, enter a Github username and API Token.

 * Edit the docker-compose.yml file to set the database username and password. 
 * Build and run the application using the following docker-compose commands from the root directory of the application:
  
  * docker-compose build
  * docker-compose up -d

 * Alternatively, to run the app outside of docker-compose, you can use the following commands:

  * rails db:run_if_no_db && rails db:create_db_with_public_data
  * rails db:migrate
  * rails assets:precompile RAILS_ENV=production
  * rails server -e production

 * Once the application is built and started, it will be available on port 3000. Please note that the docker-compose file also uses nginx and certbot to serve the application using SSL/Port 443. Please remove these entries if not needed. 
 
 * A default user account with administrator privileges is created automatically. The credentials are admin@admin-user.org/admin-password

 * The application also allows a default domain name to be set. Only users that have email addresses associated with that domain will be able to be administrators of the site. To set this:
 
  * Create an organization entry for your organization. Note the slug that is created for this organization
  * Navigate to the settings page (<online catalog URL>/settings)
  * Create a new setting called 'Default Organization'. The value for this setting will be the slug for your organization.
  
.. _ESRI: https://www.esri.com/