Sustainable Development Goals
=============================

.. meta::
   :description lang=en:

The SDGs comprise 17 goals and 169 targets representing global priorities 
for investment in order to achieve sustainable development. The SDGs were set 
in 2015 by the United Nations General Assembly and intended to be achieved by 
the year 2030.

SDGs each have specific Targets. These are measurable goals that allow people
to determine if an SDG has been achieved.

In the online catalog SDGs are linked to specific :doc:`products <product>`. These links help
users understand the SDGs that the products are oriented to addressing. 

SDG targets are also linked to :doc:`use cases <use_case>`. A user can see what use 
cases are connected to a particular target and for a specific use case, can see
which SDGs and SDG targets that the use case is addressing.

Visit the :doc:`Filters page <filter>` to see how the filters can be used to create
powerful searches that connect SDGs to Use Cases, Workflows, Building Blocks, and 
Products.
