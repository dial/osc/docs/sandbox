Workflows
=========

.. meta::
   :description lang=en:

A workflow is a generic business process that contributes to :doc:`Use Cases <use_case>` across multiple sectors and can be developed as a set of organizational capabilities.

Characteristics
 * Common business process used to help an organization carry out its overall function
 * Applicable to multiple Use Cases in various sectors
 * Powered by one or more ICT Building Blocks

Each WorkFlow includes a description, sector-specific examples, and mappings to Use Cases defined 
in the catalog. The list of WorkFlows is not exhaustive, and more will be added in future releases.

Workflows are mapped to :doc:`Building Blocks <building_block>` to show the specific technology 
functions that must be implemented in order to accomplish the business function. 

Visit the :doc:`Filters page <filter>` to see how the filters can be used to create
powerful searches that connect SDGs to Use Cases, Workflows, Building Blocks, and 
Products.