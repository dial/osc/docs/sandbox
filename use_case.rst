Use Cases
=========

.. meta::
   :description lang=en:

A Use Case is the steps that an individual or system will undertake in order to achieve a business objective.

Characteristics
 * User-centric description of the steps or user journeys required to deliver an outcome
 * Identifies one or more SDG Targets as its business objective
 * Describes the generic WorkFlows and business processes involved in each step in the Use Case
 * Typically sector-specific
 * Able to be improved through digital technology

Each Use Case model describes primary actors, key steps involved in achieving a 
specified business objective, descriptions of the WorkFlows and ICT Building Blocks 
involved in each step, and mappings to SDG Targets. The list of Use Case models is 
not exhaustive and more will be added in future releases.

In the online catalog, use cases are linked to :doc:`SDG targets <sdg>` to show which development
targets the use case is designed to address. A use case is also linked to :doc:`workflows <workflow>` to 
show what business processes are needed to implement that use case.

Visit the :doc:`Filters page <filter>` to see how the filters can be used to create
powerful searches that connect SDGs to Use Cases, Workflows, Building Blocks, and 
Products.
