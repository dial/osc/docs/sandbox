Building Blocks
===============

.. meta::
   :description lang=en:

Building blocks are enterprise-ready, reusable software components providing key functionality 
facilitating generic WorkFlows across multiple sectors.

Characteristics
 * Reusable software components
 * Can be open-source, commercial off-the-shelf (COTS), or freely available with open access to data
 * Facilitates one or more generic WorkFlows
 * Applicable to multiple UseCases across multiple sectors
 * Interoperable with other ICT Building Blocks
 * Designed for scalability and extensibility
 * Standards-based

Each ICT Building Block page includes a description, key digital functionalities, 
sector-specific examples, example software products, and mappings to WorkFlows 
defined in the catalog. 

The list of ICT Building Blocks links to :doc:`software products <product>`, which are 
candidate products that meet some or all of the criteria for that building block. These  
mappings are not exhaustive and more will be added in future releases. Please note 
that the software product examples are for illustrative purposes only. Further mapping 
and ranking of existing products based on maturity, sustainability and applicability to 
the ICT Building Blocks will be addressed over time.

Visit the :doc:`Filters page <filter>` to see how the filters can be used to create
powerful searches that connect SDGs to Use Cases, Workflows, Building Blocks, and 
Products.
