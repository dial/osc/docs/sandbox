Products
========

.. meta::
   :description lang=en:

This catalog contains an extensive list of digital goods that are designed to achieve
the SDGs. These products are all open-source and can be freely used. The list of candidate
products in the catalog are sourced from several organizations, including DIAL's Open source
center, Digital Square, and Unicef. 

The products listing page shows many different attributes of the product. Information on how to
read these product cards can be found :doc:`here <product_cards>`. Clicking on the card will
load a page with detailed information about the product. 

Each product page in the catalog provides a description of the product, a link to the source 
code repository, data on the maturity of the product, which SDGs the product is designed to 
address, any building blocks that this product may meet the criteria for. If applicable, it 
will also show any other products that this product interoperates with. 

For select products in the catalog, it is possible to automatically launch an instance of that 
product for evaluation purposes. For more information, see the :doc:`product launcher <product_launcher>` page.

Visit the :doc:`Filters page <filter>` to see how the filters can be used to create
powerful searches that connect SDGs to Use Cases, Workflows, Building Blocks, and 
Products.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Reading the Product Cards

   product_cards

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Product Launchers

   product_launcher
