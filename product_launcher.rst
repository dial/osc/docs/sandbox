Product Launchers
=================

.. meta::
   :description lang=en:

For a subset of products in the online catalog, it is possible to create and launch
an instance of that product on an AWS or Digital Ocean server. This allows a user
of the catalog to test and evaluate the product directly. 

Products that can be auto-launched have an launch icon:

.. image:: img/fa-rocket.png
   :width: 100

To launch an instance of a product, navigate to the product page. At the bottom of the page
is a section 'Automatic Launch'. The user must enter an identifier for the instance of the product.
The product name will be appended to this identifier. For example, if the identifier is 'Test' and the
product is iHRIS, the name of the instance will be 'Test-iHRIS'.

The user must then specify the platform that the instance should be created on. Currently, the
catalog supports AWS and Digital Ocean. Additional platforms will be added in the future. The user
must have an account on this platform and create an access token (see below). Once the user has 
specified the platform and access token, select Launch and the product will be created. 

The user can see information about the launched instance of the product on the Deploys page. 
This page provides information about the status of the instance, any messages, and the IP address
of the new server. The deploys page also allows the user to create an ssh login to the server. 

Access Tokens
 * Click this link for information on how to `create an access token on Digital Ocean`_
 * Click this link for information on how to `create an access token on AWS`_

.. _create an access token on Digital Ocean: https://www.digitalocean.com/docs/api/create-personal-access-token/

.. _create an access token on AWS: https://docs.aws.amazon.com/general/latest/gr/managing-aws-access-keys.html