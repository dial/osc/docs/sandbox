Reading the Product Cards
=========================

.. meta::
   :description lang=en:

The product cards contain a wide range of information about a software product, available at a
glance. This data is displayed in icon format and more detailed information about any of these
icons can be seen by hovering over the icon. 

The following diagrams show how the cards are laid out and what information is shown. 

.. image:: img/product-legend1.png
   :width: 600

.. image:: img/product-legend2.png
   :width: 600


The footer of the product card contains 3 rows. Hover over any icon to get more information 
about the data that is displayed. The first two rows may contain information about what 
Sustainable Development Goals this product addresses, which building blocks the products are a 
candidate for, product maturity information as measured by DIAL's Open Source Center or Digital Square, 
and what other products this product interoperates with. The bottom row will always contain 
information about data sources for this product - currently we retreive product data from the DIAL 
Open Source Center, Digital Square, and Unicef's Global Goods Candidate list.
