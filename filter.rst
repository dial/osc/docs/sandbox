Using the filters
=================

.. meta::
   :description lang=en:

The online catalog offers powerful filtering capabilities that allow the user to see how
SDGs, Use Cases, Workflows, Building Blocks and Products are connected. The filter also
allows users to 

The filter is on the left side of the screen:

.. image:: img/filter.png
   :width: 400

The following diagrams provide information on how to use the filters and what the various icons
mean in the filter section. In the case of Products, there are many ways that they can be filtered. 
The user can see which products have maturity assessments, which ones can be :doc:`automatically launched <product_launcher>`, 
and what the source of the product data is (DIAL Open Source Center, Digital Square, or Unicef)

.. image:: img/filter-help.png
   :width: 600

When filters are selected by the user, the list of SDGs, Use Cases, Workflows, Building Blocks
and Products will be limited to those that meet the criteria set in the filter. The top navigation
bar will show the number of items that meet those criteria:

.. image:: img/filter-counter.png
   :width: 600

At the top of the screen, the user will also see a detailed list of all of the filters that have been 
set. The user can remove filters here, as well as in the main filtering section.

.. image:: img/filter-active.png
   :width: 600
